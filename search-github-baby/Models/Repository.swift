//
//  Repository.swift
//  search-github-baby
//
//  Created by admin on 31.10.2018.
//  Copyright © 2018 newrandomuser. All rights reserved.
//

import Foundation

struct Repository: Decodable {
    let id: Int?
    let name: String?
    let full_name: String?
    let html_url: String?
    let description: String?
    let url: String?
    let stargazers_count: Int?
    let forks: Int?
    let watchers: Int?
    let license: License?
    let language: String?
    let updated_at: String?
}
