//
//  Repositories.swift
//  search-github-baby
//
//  Created by admin on 31.10.2018.
//  Copyright © 2018 newrandomuser. All rights reserved.
//

import Foundation

struct Repositories: Decodable {
    let total_count: Int
    let incomplete_results: Bool
    let items: [Repository]
}
