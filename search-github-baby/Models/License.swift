//
//  License.swift
//  search-github-baby
//
//  Created by admin on 31.10.2018.
//  Copyright © 2018 newrandomuser. All rights reserved.
//

import Foundation

struct License: Decodable {
    let key: String?
    let name: String?
}
